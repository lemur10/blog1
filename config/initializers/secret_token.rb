# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Blogger::Application.config.secret_key_base = '2f7f9d5007a40a9238204528728834bb31119df5393d30fd61ad60b8269e2254c69824bed75bb7aae6302e142cf764772d8eb551f782ace2e08c7bb3c88665c2'
