Blogger::Application.routes.draw do
  devise_for :users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  #get "articles/index"
  root 'articles#index'

  resources :articles
 
  resources :articles do
    resources :comments
  end

end
