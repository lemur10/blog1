class Article < ActiveRecord::Base
	has_many :comments, dependent: :destroy
	has_attached_file :image
	validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png"]
	#validates :title, presence: true 
	#validates :body, presence: true 
end
