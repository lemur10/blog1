class Comment < ActiveRecord::Base
  belongs_to :article
  validates :author_name, presence: true
  validates :body, length: { minimum: 2, maximum: 20 }
end
