ActiveAdmin.register Article do
permit_params :title, :body
show do |article|
      attributes_table do
        row :id
        row :title
        row :body
        row :created_at
        row :image do
          image_tag(article.image.url)
        end
      end
    end
  
end
